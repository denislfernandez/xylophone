import 'package:audioplayers/audio_cache.dart' as audio;
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  final audioPlayer = audio.AudioCache();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Xylophone(),
        ),
      ),
    );
  }
}

class Xylophone extends StatelessWidget {
  final audioPlayer = audio.AudioCache();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Key(Colors.red, 'sounds/note1.wav', audioPlayer),
        Key(Colors.blue, 'sounds/note2.wav', audioPlayer),
        Key(Colors.green, 'sounds/note3.wav', audioPlayer),
        Key(Colors.yellow, 'sounds/note4.wav', audioPlayer),
        Key(Colors.pink, 'sounds/note5.wav', audioPlayer),
        Key(Colors.purple, 'sounds/note6.wav', audioPlayer),
        Key(Colors.orange, 'sounds/note7.wav', audioPlayer),
      ],
    );
  }
}

class Key extends StatelessWidget {
  final Color color;
  final String audioFileName;
  final audio.AudioCache audioPlayer;
  Key(this.color, this.audioFileName, this.audioPlayer);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
          color: color,
          onPressed: _playAudioFile,
          child: null),
    );
  }

  _playAudioFile() {
    audioPlayer.play(audioFileName);
  }
}
